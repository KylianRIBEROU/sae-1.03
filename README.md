# SAE 1.03 - Installer un système

## Objectifs : 

Installer une distribution de linux sur un ordinateur, et apprendre à la configurer efficacement.

##  Prérequis : 

- Une clef USB vierge, de minimum 4 Giga-Octets environ
- Un ordinateur sur lequel on souhaite installer notre distribution Linux ( dans mon cas, j'installe une distribution Xubuntu 22.04 )

# Préparer l'installation de Xubuntu

Premièrement, il faut mettre l'image disque de Xubuntu sur la clef USB.

Pour cela, nous avons besoin du logiciel Rufus, qui nous permettra de formater notre clef et de la passer en mode boot avec l'image disque de Xubuntu. Pour télécharger Rufus, cliquez [ici](https://rufus.ie/fr/).

Nous avons aussi besoin de télécharger le fichier ISO sur le site correspondant à celui-ci. Pour Xubuntu, cliquez [ici](http://ftp.free.fr/mirrors/ftp.xubuntu.com/releases/20.04/release/). Puis nous téléchargeons l'iso de notre choix, la version la plus récente au moment de l'écriture de ce fichier étant la Xubuntu-20.04.5.

Maintenant, nous devons "mettre" cette image sur notre clef USB. On utilisera Rufus et appliquera les instructions suivantes :

- Dans le champ "Périphérique", choisissez la clef.
- Dans le champ "Type de démarrage", choisissez "Image disque ou ISO" et sélectionnez l'ISO de Xubuntu téléchargée précédemment.
- Dans le champ "Schéma de partition", choisissez GPT, MBR étant un schéma de partitionnement plus ancien offrant moins de possibilités en terme de partionnement de disque.
- Dans le champ "Système de destination", choisissez BIOS ou UEFI pour avoir le BIOS traditionnel.
Pour cette installation de Xubuntu, les autres champs sont laissés avec leur valeur par défault.

Enfin, cliquer sur Démarrer. **ATTENTION**, cette opération formatera la clef USB. Il est donc important d'avoir fait une copie du contenu au préalable ou d'avoir pris une clef vierge au début.

Bravo, la clef USB est configurée en mode BOOT et est désormais prête à installer le système sur notre ordinateur. 

# Installer Xubuntu

## Accéder au BIOS de l'ordinateur :

Pour installer l'image disque, il nous faut un endroit où lancer la clef !

Cet endroit, c'est le BIOS de l'ordinateur, son système de base. Pour y accéder, tout dépend de la marque de votre ordinateur. Ce BIOS est accessible lors du démarrage de votre ordinateur en pressant une certaine touche. Par exemple, pour un ASUS, il faut rester appuyé longuement sur F2, ou alors F10 pour un HP.

Bravo, nous sommes dans le BIOS. Maintenant nous devons aller dans le menu nommé "BOOT". Dans mon cas, sur un ASUS, on choisit notre clef depuis le sous-menu BOOT OVERRIDE pour l'éxecuter. Le menu de la clef USB s'ouvre alors : il faut faire ENTREE sur la ligne `Try or Install Xubuntu`.

Il est aussi possible d'accéder au menu de boot directement en démarrant son pc et en restant appuyé sur "ECHAP" ( la touche change selon la marque de l'ordinateur, l'exemple ici est donné pour un ASUS ).

## Débuter l'installation :

Le menu d'installation Xubuntu s'affiche alors. Il suffit de suivre les étapes affichées à l'écran mais nous allons quand même détailler les étapes pour ne pas se tromper.

- Au début, on choisit la langue de notre système d'exploitation et si on souhaite essayer ou installer Xubuntu. la fonction essayer est plus souvent utilisée pour intervenir temporairement sur un pc avec des logiciels live, comme GPARTED par exemple, donc on choisit d'installer Xubuntu.
- On configure ensuite la disposition du clavier. On peut laisser le clavier par défaut si on a choisi la langue française dans l'étape précédente.
- Ensuite, on doit configurer le **Secure Boot**. Le Secure Boot est un système de sécurité ne permettant pas à n'importe qui d'aller dans le bios de votre pc et d'y faire ce qu'il y veut. Il est donc très utile de le configurer et de paramétrer un mot de passe.
- Enfin, la dernière étape la plus importante : le menu de partition. A cette étape, on choisit si l'on souhaite installer Xubuntu en Dual Boot, donc à coté d'un OS Windows sur un disque dur partionné en deux, ou tout seul. Pour mon installation, j'ai décidé d'installer Xubuntu sur l'entièreté du disque et donc de partir de 0. On choisit donc l'option `Effacer le disque et installer Xubuntu`.

Voilà, la configuration de Xubuntu est terminée et l'installation commence ! Il ne reste plus qu'à attendre et à redémarrer l'ordinateur si demandé.

Maintenant, il ne reste plus qu'a voir dans une deuxième partie comment configurer son système pour en faire un environnement de travail efficace !


# Construire l'environnement de travail

Maintenant, la dernière partie ! Nous allons installer différents logiciels pour pouvoir programmer le plus rapidement possible : 

- **Visual Studio Code** : c'est L'IDE (environnement de travail intégré) que nous installerons sur cet ordinateur car c'est l'IDE le plus polyvalent et facile à prendre en main. Pour cela, il suffit de télécharger le paquet [ici](https://code.visualstudio.com/download). Ensuite, ouvrir le terminal avec le raccourci `ctrl+alt+t` et se déplacer avec la commande `cd` dans l'endroit ou est situé le paquet de Visual Studio Code. Enfin, rentrer la commande `sudo dpkg -i code_*.deb` et voilà, l'IDE s'installe ! Il ne reste plus qu'à effectuer la commande `code` pour le lancer.

- **Python** : on installe la version 3 de Python avec la commande `sudo apt install python3`. Dans mon cas, j'ai aussi installé le paquet pip avec la commande `sudo apt install python3-pip` pour pouvoir installer et utiliser des librairies dans mes programmes Python.

- **Java** : on installe le paquet permettant de compiler et d'éxecuter des programmes Java. Pour le compileur, on éxecute dans le terminal la commande `sudo apt install open-jdk` et pour Java en général on éxecute `sudo apt install default-jre`.

- **Docker** : Docker étant une machine virtuelle permettant d'éxecuter du code accessible en ligne, une connexion internet stable est nécessaire pour l'utiliser. Il suffit d'installer le paquet docker.io avec la commande `sudo apt install docker.io`. On peut aussi installer le paquet docker.compose : `sudo apt install docker.compose` si l'on souhaite travailler avec plusieurs conteneurs ( Docker est une API pour gérer des conteneurs, qui sont des éxecutables pour lancer des programmes).

N'oubliez pas d'éxecuter avant et après une installation la commande `sudo apt update` pour mettre à jour les paquets.

Voilà, ce tutoriel d'installation est terminé ! Il ne reste plus qu'à configurer un compte Gitlab pour partager des fichiers / dossiers en ligne avec différentes commandes, les plus connues étant `git clone` ou encore `git push` et votre ordinateur est désormais un environnement de travail adapté pour débuter en programmation !






